output "droplet_ip_addresses" {
  value = digitalocean_droplet.web_terraform_homework[*].ipv4_address
}
