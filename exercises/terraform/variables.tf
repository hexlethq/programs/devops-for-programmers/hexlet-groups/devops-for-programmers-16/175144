variable "do_token" {}

variable "public_key" {
  description = "The public key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChx2Y1JLhVJzdaACJowVuhYl0MxhF44qKL3ICMEiDYTh4lKs6Ll0sS+3zTv+ljXiTl+VLnEO9CYzZXe1dWbtpHzEgg59N3j0gb9rDItThhCY9GR/Q/bof79HhZGtW1gja8Pdj5zHQmGYT7KzK3L3JMR7ndqUAo5vVDR+fPRNRUf2miIpOkp2CROCeLKnnar8edYZqyEsjAK5QVHPE1dudg3urnsAbj5hSYAQa6O18kuwO4gL/0GkmdlJeCMepTmHiuPbW47aM+TkF0LpDnIwgeNKV3VzBioiNdJohziB8/euZx38tbXiX6iNX+fYYb0JULAy2Ev0tlTP0GnnMJIjg4bXGURA0XNsC5L/yscRhZsYhcC6OMsKrUcsnxaIllHmefLD+kDI5vnuiSHRDRRFhGXbz36qQeyWSTUeqwM8tGPbIqLW0CykG/4aEYcrnHVAsvAozWCJzpvwIwCtIETtsji+WLaO5M1f/8037uUwhXCbUqCM85EFntvBQKpsPF9Ak= pavel@pavel-virtual-machine"
}

variable "number_droplets" {
  default = 2
}

variable "droplet_name" {
  description = "The Droplet name"
  default     = "web-terraform-homework"
}

variable "droplet_image" {
  description = "The Droplet image ID or slug"
  default     = "docker-20-04"
}

variable "region" {
  description = "The region to start in"
  default     = "ams3"
}

variable "droplet_size" {
  description = "The unique slug that indentifies the type of Droplet"
  default     = "s-1vcpu-1gb"
}

variable "loadbalancer_name" {
  description = "The Load Balancer name"
  default     = "lb-terraform-homework"
}

variable "root_domain_name" {
  description = "The name of the domain"
  default     = "devops-for-programmers-prod.club"
}
