terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "terraform" {
  name       = "terraform"
  public_key = var.public_key
}

resource "digitalocean_droplet" "web_terraform_homework" {
  count  = var.number_droplets
  image  = var.droplet_image
  name   = format("${var.droplet_name}-%02d", count.index + 1)
  region = var.region
  size   = var.droplet_size
  ssh_keys = [
    digitalocean_ssh_key.terraform.fingerprint
  ]
}

# resource "digitalocean_droplet" "web_terraform_homework_02" {
#   image  = "docker-20-04"
#   name   = "web-terraform-homework-02"
#   region = "ams3"
#   size   = "s-1vcpu-1gb"
#   ssh_keys = [
#     digitalocean_ssh_key.terraform.fingerprint
#   ]
# }

resource "digitalocean_loadbalancer" "terraform_homework" {
  name   = var.loadbalancer_name
  region = var.region

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.web_terraform_homework[*].id
}

resource "digitalocean_domain" "root" {
  name = var.root_domain_name
}

resource "digitalocean_record" "terraform" {
  domain = digitalocean_domain.root.name
  type   = "A"
  name   = "terraform"
  value  = digitalocean_loadbalancer.terraform_homework.ip
}
