#!/usr/bin/env bash

# install nodejs
curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -
sudo apt update
sudo apt install -y nodejs
