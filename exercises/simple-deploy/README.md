# Деплой на один сервер

Для простоты будем использовать готовый образ докера с запакованным туда приложением — [hexletcomponents/devops-example-app](https://hub.docker.com/r/hexletcomponents/devops-example-app). Это простое веб-приложение, оно использует переменную окружения `SERVER_MESSAGE` для вывода на страницу сообщения.

## Ссылки

* [Документация Ansible](https://docs.ansible.com/ansible/latest/index.html)
* [DigitalOcean Documentation](https://docs.digitalocean.com/)
* [Encrypting content with Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html)
* [ansible-valut cli](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html)

## Задачи

* Попробуйте запустить *devops-example-app* локально следуя инструкциям на Docker Hub. Поэкспериментируйте, передавая разные значения переменной окружения `SERVER_MESSAGE` и посмотрите как изменяется вывод на странице приложения.
* Подключитесь по SSH к серверу на Digital Ocean и запустите вручную *devops-example-app*. Проверьте в браузере по IP-адресу сервера и доменному имени, что приложение работает.
* Реализуйте *Ansible Playbook* для деплоя *devops-example-app* на созданный ранее сервер. Зашифруйте строку `Hexlet Awesome Server` в переменную окружения `SERVER_MESSAGE` с помощью Ansible Vault и используйте её значение в приложении.
* Задеплойте приложение. Проверьте, открывается ли приложение в браузере и что по имени домена на странице сообщение.

При сдаче домашней работы, укажите в issue ссылку на задеплоенное приложение: http://<адрес>:<порт>.
