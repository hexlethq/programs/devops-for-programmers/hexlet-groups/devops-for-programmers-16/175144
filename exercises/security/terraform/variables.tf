variable "do_token" {
  description = "This is the DO API token"
  type        = string
  sensitive   = true
}

variable "public_key" {
  description = "The public key"
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYPVfz8LvAeeQKv38PtIFbNWiyIxRYGFf6AKT3nnOOtyCjbsuJpdL16183lPJfBnLlPnbvf66Snogf4+mZ6UNo4AhQvKkJgHJ2kP2y7ee6yiSy4XofiB1sNJOiQWTNa4PvKSWywr67gIdMLcDJQ8TqoUFZb1zu3m5BrWDJz2ljGbSzzMkviPbAW1WJJrXB21sNW5AFm8M9GqrpptYrgt4YV7S4qx8uZvaSGDLbWY8PKThH4VuzwlQXkTSPLKZJMsZHj4y36Ektf92pFWv4WAhh8ifYoYyEPJWB6TklN4rGXBJqIzoNbLJ0NulAKuQwOGKlqoT7tVOs0uD4ufjSlIG+SFa0NxQir2Z8ijOBOfwy6aVkT9sJ6x3FVX9cPEbAaE03oMw8KBqYDmti0xTYS7UVjLklUseRLahzRbmWzoUyxP3kNoezrhp1F7+IZPykI22quktprRfHF1F5R/uMipATzeukfRwg9l9UtlS7YlCgNOd/4z60as128w3SBb9iZsXE1bGrReBPgjGM3LaCdKO5jDxKzvCoW0VR7toaugCGJwVTRUsWbZEBzR5Oqoz9TldPKrRHQ1wijW4mwUX+rk62FPSrnyY9tQN9BYTABXydhwIOV2tp5jdvG6xPg7MvPlNkIyKaIOO0rbCwWhW/2lqeStQWJWVwF9e5Smg1ebVOfw== root@infrastructure"
}

variable "number_droplets" {
  type    = number
  default = 2
}

variable "droplet_name" {
  description = "The Droplet name"
  type        = string
  default     = "web-security-homework"
}

variable "droplet_image" {
  description = "The Droplet image ID or slug"
  type        = string
  default     = "docker-20-04"
}

variable "region" {
  description = "The region to start in"
  type        = string
  default     = "ams3"
}

variable "droplet_size" {
  description = "The unique slug that indentifies the type of Droplet"
  type        = string
  default     = "s-1vcpu-1gb"
}

variable "loadbalancer_name" {
  description = "The Load Balancer name"
  type        = string
  default     = "lb-security-homework"
}

variable "root_domain_name" {
  description = "The name of the domain"
  type        = string
  default     = "devops-for-programmers-prod.club"
}

variable "vpc_name" {
  description = "A name for the VPC"
  type        = string
  default     = "vpc-security-homework"
}

variable "vpc_ip_range" {
  description = "The range of IP addresses for the VPC in CIDR notation"
  type        = string
  default     = "10.10.10.0/24"
}

variable "bastion_name" {
  description = "The Bastion Droplet name"
  type        = string
  default     = "bastion-security-homework"
}
