data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}

resource "digitalocean_vpc" "security_homework" {
  name     = var.vpc_name
  region   = var.region
  ip_range = var.vpc_ip_range
}

resource "digitalocean_droplet" "web" {
  count              = var.number_droplets
  image              = var.droplet_image
  name               = format("${var.droplet_name}-%02d", count.index + 1)
  region             = var.region
  size               = var.droplet_size
  private_networking = true
  vpc_uuid           = digitalocean_vpc.security_homework.id
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
}

resource "digitalocean_droplet" "bastion" {
  image              = var.droplet_image
  name               = var.bastion_name
  region             = var.region
  size               = var.droplet_size
  private_networking = true
  vpc_uuid           = digitalocean_vpc.security_homework.id
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
}

resource "digitalocean_certificate" "cert" {
  name    = "domain-cert"
  type    = "lets_encrypt"
  domains = ["security.${var.root_domain_name}"]
}

resource "digitalocean_loadbalancer" "public" {
  name     = var.loadbalancer_name
  region   = var.region
  vpc_uuid = digitalocean_vpc.security_homework.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.web[*].id
}

resource "digitalocean_record" "security" {
  domain = var.root_domain_name
  type   = "A"
  name   = "security"
  value  = digitalocean_loadbalancer.public.ip
}

resource "digitalocean_firewall" "bastion" {
  name        = "bastion-fw"
  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "22"
    destination_addresses = [var.vpc_ip_range]
  }
}

resource "digitalocean_firewall" "web" {
  name        = "web-fw"
  droplet_ids = digitalocean_droplet.web[*].id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "1-65535"
    source_addresses = [var.vpc_ip_range]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = [var.vpc_ip_range]
  }

  inbound_rule {
    protocol         = "udp"
    port_range       = "1-65535"
    source_addresses = [var.vpc_ip_range]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = [var.vpc_ip_range]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}
