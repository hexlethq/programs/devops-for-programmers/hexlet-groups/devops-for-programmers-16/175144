output "droplets_web" {
  value = digitalocean_droplet.web[*]
}

output "bastion" {
  value = digitalocean_droplet.bastion
}
