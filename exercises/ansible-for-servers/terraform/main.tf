terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "terraform" {
  name       = "terraform"
  public_key = var.public_key
}

resource "digitalocean_droplet" "web" {
  count  = var.number_droplets
  image  = var.droplet_image
  name   = format("${var.droplet_name}-%02d", count.index + 1)
  region = var.region
  size   = var.droplet_size
  ssh_keys = [
    digitalocean_ssh_key.terraform.fingerprint
  ]
}

resource "digitalocean_loadbalancer" "public" {
  name   = var.loadbalancer_name
  region = var.region

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.web[*].id
}

resource "digitalocean_domain" "root" {
  name = var.root_domain_name
}

resource "digitalocean_record" "afs" {
  domain = digitalocean_domain.root.name
  type   = "A"
  name   = "afs"
  value  = digitalocean_loadbalancer.public.ip
}
