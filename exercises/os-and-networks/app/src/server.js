const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('./data/db.json');
const middlewares = jsonServer.defaults();
const db = require('../data/db.json');
const _ = require('lodash')
const port = process.env.PORT || 3000;

server.use(jsonServer.bodyParser)

server.post('/detailInfo', (req, res) => {
    console.log(`====== Call method '/detailInfo' ======`);
    const userId = req.body['userId'];
    console.log(`userId in request '${userId}'`);
    const result = db.detailInfo.find(detailInfo => {
        return detailInfo.userInfo.login == userId;
    });

    res.status(200).jsonp(result);
});

server.post('/systemroles', (req, res) => {
    console.log(`====== Call method '/systemroles' ======`);
    console.log(`Request '${JSON.stringify(req.body)}'`);

    const db = router.db;
    insert(db, 'systemroles', req.body);

    const result = {};
    res.status(200).jsonp(result);
});

/**
 * Checks whether the id of the new data already exists in the DB
 * @param {*} db - DB object
 * @param {String} collection - Name of the array / collection in the DB / JSON file
 * @param {*} data - New record
 */
function insert(db, collection, data) {
    const table = db.get(collection);
    if (_.isEmpty(table.find(data).value())) {
        table.push(data).write();
    }
}

server.use(middlewares);
server.use(router);

server.listen(port, () => {
    console.log(`JSON Server is running on port ${port}`);
});
